import { Routes, RouterModule } from '@angular/router';

import { HeroesComponent }      from './hero/heroes.component';
import { DashboardComponent }      from './dashboard/dashboard.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';

const appRoutes: Routes = [
    {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'heroes',
    component: HeroesComponent
  },
  {
  path: 'detail/:id',
  component: HeroDetailComponent
},
  {
  path: 'dashboard',
  component: DashboardComponent
}
];

export const routing = RouterModule.forRoot(appRoutes);
